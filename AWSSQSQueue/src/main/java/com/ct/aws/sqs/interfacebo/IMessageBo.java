package com.ct.aws.sqs.interfacebo;

import com.ct.aws.sqs.exception.CustomException;
import com.ct.aws.sqs.model.MessageBean;

public interface IMessageBo {

	String addMssgToQueue(MessageBean msg) throws CustomException;
}

package com.ct.aws.sqs.impl.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.ct.aws.sqs.exception.CustomException;
import com.ct.aws.sqs.interfacebo.IMessageBo;
import com.ct.aws.sqs.model.MessageBean;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class MessageBo implements IMessageBo {

	@Autowired
	private AmazonSQS amazonSQS;

	@Autowired
	private CreateQueueResult createQueueResult;

	private String queueURL;

	private static final Logger SL4JLOGGER = LoggerFactory.getLogger(MessageBo.class);

	@Override
	public String addMssgToQueue(MessageBean msg) throws CustomException {
		if (createQueueResult != null) {
			queueURL = createQueueResult.getQueueUrl();

			SL4JLOGGER.info("MessageBo.addMssgToQueue()" + createQueueResult);
			SL4JLOGGER.info("Queue Url : " + queueURL);

			if (queueURL != null && !queueURL.equals("")) {
				return addMessage(queueURL, amazonSQS, msg);
			}
		}
		else {
			throw new CustomException("Queue Not Created");
		}
		return null;
	}

	private String addMessage(String queueURL, AmazonSQS sqsClient, MessageBean msg) {

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonInString = mapper.writeValueAsString(msg);
			SendMessageResult result = sqsClient.sendMessage(new SendMessageRequest(queueURL, jsonInString));
			if (result.getMessageId() != null) {
				return "Successfully Pushed Message to Queue";
			}
		}
		catch (Exception e) {
			SL4JLOGGER.info(e.getMessage());
			SL4JLOGGER.info(e.getStackTrace().toString());
		}

		return null;
	}

	public void setAmazonSQS(AmazonSQS amazonSQS) {
		this.amazonSQS = amazonSQS;
	}

	public void setCreateQueueResult(CreateQueueResult createQueueResult) {
		this.createQueueResult = createQueueResult;
	}

}

package com.ct.aws.sqs.exception;

public class CustomException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mssg;

	public CustomException(String mssg) {
		super(mssg);
		this.mssg = mssg;
	}

	public CustomException() {
	}

	public String getMssg() {
		return mssg;
	}

}

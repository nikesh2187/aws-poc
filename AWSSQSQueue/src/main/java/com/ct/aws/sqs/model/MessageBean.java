package com.ct.aws.sqs.model;

public class MessageBean {

	private int id;
	private String fileName;
	private String fileLocation;

	public MessageBean() {
	}

	public MessageBean(int id, String fileName, String fileLocation) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.fileLocation = fileLocation;
	}

	public int getId() {
		return id;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

}

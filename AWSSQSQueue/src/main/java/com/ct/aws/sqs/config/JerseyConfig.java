/**
 * 
 */
package com.ct.aws.sqs.config;

/**
 * @author Shwetha
 *
 */
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.ct.aws.sqs.service.MessageService;


@Component
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(MessageService.class);
	}

}

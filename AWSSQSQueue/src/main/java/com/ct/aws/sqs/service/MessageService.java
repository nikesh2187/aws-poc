package com.ct.aws.sqs.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ct.aws.sqs.exception.CustomException;
import com.ct.aws.sqs.interfacebo.IMessageBo;
import com.ct.aws.sqs.model.MessageBean;


@Component
@Path("/messages")
public class MessageService {

	private static final Logger SL4JLOGGER = LoggerFactory.getLogger(MessageService.class);

	@Autowired
	private IMessageBo messageBO;

	public void setMessageBO(IMessageBo messageBO) {
		this.messageBO = messageBO;
	}

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addMessageToQueue(MessageBean msg) {
		ResponseBuilder rb = null;
		SL4JLOGGER.info("MessageService.addMessageToQueue()");
		String response;
		try {
			response = messageBO.addMssgToQueue(msg);
			rb = Response.ok(response, MediaType.TEXT_PLAIN);
		}
		catch (CustomException e) {
			rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage());
		}

		return rb.build();
	}

}

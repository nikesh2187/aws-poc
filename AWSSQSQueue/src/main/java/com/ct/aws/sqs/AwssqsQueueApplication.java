package com.ct.aws.sqs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;

@SpringBootApplication
@EnableAutoConfiguration
@Configuration
@ComponentScan({ "com.ct.aws.sqs" })
public class AwssqsQueueApplication extends SpringBootServletInitializer {
	
	private static final Logger SLF4JLOGGER = LoggerFactory.getLogger(AwssqsQueueApplication.class);

	@Value("${aws.queueName}")
	private String queueName;

	@Value("${aws.access_key_id}")
	private String awsAccessKey;

	@Value("${aws.secret_access_key}")
	private String awsSecretAccessKey;

	public static void main(String[] args) {
		SpringApplication.run(AwssqsQueueApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AwssqsQueueApplication.class);
	}
	
	@Bean
	public AmazonSQS amazonSQS() {
		SLF4JLOGGER.info("awsAccessKey " + awsAccessKey);
		SLF4JLOGGER.info("awsSecretAccessKey " + awsSecretAccessKey);

		AWSCredentials credentials = new BasicAWSCredentials(awsAccessKey, awsSecretAccessKey);

		AmazonSQS sqsClient = new AmazonSQSClient(credentials);
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		sqsClient.setRegion(usWest2);
		return sqsClient;
	}

	@Bean
	public CreateQueueResult createQueueResult() {
		CreateQueueResult createQueueResult = null;
		try {
			AmazonSQS sqsClient = amazonSQS();

			if (queueName != null && queueName.trim().length() != 0 && sqsClient != null) {
				CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName.trim());
				createQueueResult = sqsClient.createQueue(createQueueRequest);
			}
		}
		catch (AmazonClientException ace) {
			SLF4JLOGGER.info("Caught an AmazonClientException, which means the client encountered " + "a serious internal problem while trying to communicate with SQS, such as not " + "being able to access the network.");
			SLF4JLOGGER.info("Error Message: " + ace.getMessage());
		}

		return createQueueResult;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
		PropertySourcesPlaceholderConfigurer c = new PropertySourcesPlaceholderConfigurer();
		c.setLocation(new ClassPathResource("application.properties"));
		return c;
	}

	public String getQueueName() {
		return queueName;
	}

	public String getAwsAccessKey() {
		return awsAccessKey;
	}

	public String getAwsSecretAccessKey() {
		return awsSecretAccessKey;
	}
}

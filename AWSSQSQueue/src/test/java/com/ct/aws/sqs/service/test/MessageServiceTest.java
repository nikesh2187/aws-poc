package com.ct.aws.sqs.service.test;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ct.aws.sqs.exception.CustomException;
import com.ct.aws.sqs.impl.bo.MessageBo;
import com.ct.aws.sqs.model.MessageBean;
import com.ct.aws.sqs.service.MessageService;

public class MessageServiceTest {

	private MessageService messageService;

	MessageBo messageBoMock;

	MessageBean msgBeanInst;

	@Before
	public void setup() {
		messageBoMock = Mockito.mock(MessageBo.class);
		messageService = new MessageService();
		messageService.setMessageBO(messageBoMock);
		msgBeanInst = new MessageBean();
	}

	@Test
	public void testAddMssgToQueue_success() throws CustomException {

		String response = "Pushed Successfully";
		Mockito.when(messageBoMock.addMssgToQueue(msgBeanInst)).thenReturn(response);
		Response res = messageService.addMessageToQueue(msgBeanInst);
		Assert.assertEquals(200, res.getStatus());
	}

	@Test
	public void testAddMssgToQueue_unsuccessful() throws CustomException {

		Mockito.doThrow(new CustomException()).when(messageBoMock).addMssgToQueue(msgBeanInst);
		Response res = messageService.addMessageToQueue(msgBeanInst);
		Assert.assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), res.getStatus());

	}

}

package com.ct.aws.sqs.config.test;

import static org.junit.Assert.assertNull;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ct.aws.sqs.config.CORSFilter;

public class CORSFilterTest {

	HttpServletResponseWrapper responseWrapperMock;
	CORSFilter corsFilterInst;
	HttpServletRequest requestMock;
	HttpServletResponse responseMock;
	FilterChain filterChainMock;

	@Before
	public void setup() {
		responseMock = Mockito.mock(HttpServletResponse.class);
		responseWrapperMock = new HttpServletResponseWrapper(responseMock);
		requestMock = Mockito.mock(HttpServletRequest.class);
		corsFilterInst = new CORSFilter();
		filterChainMock = Mockito.mock(FilterChain.class);
	}

	@Test
	public void testDoFilterInternalWhenRequestsgetHeaderIsNull() throws ServletException, IOException {

		corsFilterInst.doFilterInternal(requestMock, responseMock, filterChainMock);

		assertNull(responseMock.getHeader("Access-Control-Allow-Methods"));
	}

	@Test
	public void testDoFilterInternalWhenRequestsgetHeaderIsNotNull() throws ServletException, IOException {

		Mockito.when(requestMock.getHeader("Access-Control-Request-Method")).thenReturn("GET");
		corsFilterInst.doFilterInternal(requestMock, responseWrapperMock, filterChainMock);
	}

}

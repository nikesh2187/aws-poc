package com.ct.aws.sqs.exception.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.ct.aws.sqs.exception.CustomException;

public class CustomExceptionTest {

	@Test
	public void testCustomExceptionConstructorWithArg() {

		String expectedMsg = "Custom";
		CustomException CustomExceptionTest = new CustomException(expectedMsg);
		assertEquals(CustomExceptionTest.getMssg(), expectedMsg);
	}

	@Test
	public void testCustomExceptionConstructorWithNoArg() {
		CustomException CustomExceptionTest = new CustomException();
		assertNull(CustomExceptionTest.getMssg());
	}

}

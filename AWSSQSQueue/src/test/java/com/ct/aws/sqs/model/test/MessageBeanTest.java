package com.ct.aws.sqs.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ct.aws.sqs.model.MessageBean;

public class MessageBeanTest {

	@Test
	public void testConstructorArg() {
		int expectedId = 5;
		String expectedFileName = "t1.txt";
		String expectedFileLocn = "C:\\t1.txt";

		MessageBean messageBeanInst = new MessageBean(expectedId, expectedFileName, expectedFileLocn);
		int actualId = messageBeanInst.getId();
		String msg = "Expected and actual results are not same: expectedId:" + expectedId + "actualId :" + actualId;
		assertEquals(msg, expectedId, actualId);

		String actualFileLocn = messageBeanInst.getFileLocation();
		msg = "Expected and actual results are not same: expectedFileLocn:" + expectedFileLocn + "actualFileLocn :" + actualFileLocn;
		assertEquals(msg, expectedFileLocn, actualFileLocn);

		String actualFileName = messageBeanInst.getFileName();
		msg = "Expected and actual results are not same: expectedFileName:" + expectedFileName + "actualFileName :" + actualFileName;
		assertEquals(msg, expectedFileName, actualFileName);
	}

	@Test
	public void testFileId() {
		MessageBean messageBeanInst = new MessageBean();
		int expectedId = 5;
		messageBeanInst.setId(expectedId);
		int actualId = messageBeanInst.getId();
		String msg = "Expected and actual results are not same: expectedId:" + expectedId + "actualId :" + actualId;
		assertEquals(msg, expectedId, actualId);

	}

	@Test
	public void testFileLocn() {
		MessageBean messageBeanInst = new MessageBean();
		String expectedFileLocn = "C:\\t1.txt";
		messageBeanInst.setFileLocation(expectedFileLocn);
		String actualFileLocn = messageBeanInst.getFileLocation();
		String msg = "Expected and actual results are not same: expectedFileLocn:" + expectedFileLocn + "actualFileLocn :" + actualFileLocn;
		assertEquals(msg, expectedFileLocn, actualFileLocn);

	}

	@Test
	public void testFileName() {
		MessageBean messageBeanInst = new MessageBean();
		String expectedFileName = "t1.txt";
		messageBeanInst.setFileName(expectedFileName);
		String actualFileName = messageBeanInst.getFileName();
		String msg = "Expected and actual results are not same: expectedFileName:" + expectedFileName + "actualFileName :" + actualFileName;
		assertEquals(msg, expectedFileName, actualFileName);

	}

}

package com.ct.aws.sqs.impl.bo.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.ct.aws.sqs.exception.CustomException;
import com.ct.aws.sqs.impl.bo.MessageBo;
import com.ct.aws.sqs.model.MessageBean;
import com.fasterxml.jackson.databind.ObjectMapper;

@PrepareForTest({ MessageBo.class })
@RunWith(PowerMockRunner.class)
public class MessageBOTest {

	@Mock
	CreateQueueResult CreateQueueResultMock;

	@Mock
	AmazonSQS amazonSQSMock;

	@Mock
	SendMessageResult sendMessageResultMock;

	@Mock
	ObjectMapper objectMapperMock;

	MessageBo messageBoInst;
	MessageBean msgBeanInst;

	@Test
	public void testAddMessageToQueueSuccess() throws CustomException {

		setUp();
		// Make a method call
		String expectedResult = "Successfully Pushed Message to Queue";
		String actualResult = messageBoInst.addMssgToQueue(msgBeanInst);
		String msg = "Expected and actual results are not same: expectedResult:" + expectedResult + " actualResult:" + actualResult;
		assertEquals(msg, expectedResult, actualResult);

	}

	/*
	 * @Test public void testAddMessageToQueueWhenCreateQResultIsNull() throws
	 * CustomException {
	 * 
	 * MessageBean msgBeanInst = new MessageBean(1, "TakeIt.Txt",
	 * "C:\\MyFolder");
	 * 
	 * MessageBo messageBoInst = new MessageBo();
	 * 
	 * CreateQueueResult createQueueResult =null;
	 * 
	 * messageBoInst.setCreateQueueResult(createQueueResult);
	 * 
	 * // Make a method call
	 * assertNull(messageBoInst.addMssgToQueue(msgBeanInst));
	 * 
	 * }
	 */
	@Test
	public void testAddMessageToQueueWhenQUrlIsNull() throws CustomException {

		setUp();
		// Make a method call
		String expectedResult = "Successfully Pushed Message to Queue";
		String actualResult = messageBoInst.addMssgToQueue(msgBeanInst);
		String msg = "Expected and actual results are not same: expectedResult:" + expectedResult + " actualResult:" + actualResult;
		assertEquals(msg, expectedResult, actualResult);

	}

	@Test
	public void testAddMessageToQueueForException() throws Exception {

		MessageBean msgBeanInst = new MessageBean(1, "TakeIt.Txt", "C:\\MyFolder");

		MessageBo messageBoInst = new MessageBo();

		messageBoInst.setCreateQueueResult(CreateQueueResultMock);

		// Mock CreateQueueResult
		messageBoInst.setCreateQueueResult(CreateQueueResultMock);

		Mockito.when(CreateQueueResultMock.getQueueUrl()).thenReturn("Mocked_Value_To_Return");

		PowerMockito.whenNew(ObjectMapper.class).withNoArguments().thenReturn(objectMapperMock);

		Mockito.doThrow(new RuntimeException()).when(objectMapperMock).writeValueAsString(Matchers.anyObject());

		// Make a method call
		assertNull(messageBoInst.addMssgToQueue(msgBeanInst));

	}

	@Test
	public void testAddMessageToQueueWhenMessageIdIsNull() throws CustomException {

		MessageBean msgBeanInst = new MessageBean(1, "TakeIt.Txt", "C:\\MyFolder");

		MessageBo messageBoInst = new MessageBo();

		// Mock CreateQueueResult
		messageBoInst.setCreateQueueResult(CreateQueueResultMock);
		Mockito.when(CreateQueueResultMock.getQueueUrl()).thenReturn("Mocked_Value_To_Return");

		// Mock SQSClient		
		messageBoInst.setAmazonSQS(amazonSQSMock);
		Mockito.when(amazonSQSMock.sendMessage((SendMessageRequest) Matchers.anyObject())).thenReturn(sendMessageResultMock);

		// Make a method call
		assertNull(messageBoInst.addMssgToQueue(msgBeanInst));

	}

	private void setUp() {

		msgBeanInst = new MessageBean(1, "TakeIt.Txt", "C:\\MyFolder");

		messageBoInst = new MessageBo();

		// Mock CreateQueueResult
		messageBoInst.setCreateQueueResult(CreateQueueResultMock);
		Mockito.when(CreateQueueResultMock.getQueueUrl()).thenReturn("Mocked_Value_To_Return");

		// Mock SendMessageResult
		Mockito.when(sendMessageResultMock.getMessageId()).thenReturn("Mocked_Return_ID");

		// Mock SQSClient
		messageBoInst.setAmazonSQS(amazonSQSMock);
		Mockito.when(amazonSQSMock.sendMessage((SendMessageRequest) Matchers.anyObject())).thenReturn(sendMessageResultMock);

	}

}
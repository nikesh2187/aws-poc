package com.ct.aws.sqs.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.ct.aws.sqs.AwssqsQueueApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AwssqsQueueApplication.class)
@WebAppConfiguration
public class AwssqsQueueApplicationTests {

	@Mock
	BasicAWSCredentials credentialsMock;

	@Mock
	AmazonSQSClient sqsClientMock;

	static String queueName;

	static String awsAccessKey;

	static String awsSecretAccessKey;

	@Autowired
	AwssqsQueueApplication awsSqsQueueApplicationInst;
	
	@BeforeClass
	public static void setUp(){
		readPropertyFile();
	}

	@Test
	public void testQueueName() throws Exception {
		assertEquals(queueName, awsSqsQueueApplicationInst.getQueueName());

	}

	@Test
	public void testAwsAccessKey() throws Exception {
		assertEquals(awsAccessKey, awsSqsQueueApplicationInst.getAwsAccessKey());

	}

	@Test
	public void testAwsSecretAccessKey() throws Exception {
		assertEquals(awsSecretAccessKey, awsSqsQueueApplicationInst.getAwsSecretAccessKey());

	}

	@Test
	public void testAmazonSqs() throws Exception {
		assertNotNull(awsSqsQueueApplicationInst.amazonSQS());
	}

	@Test
	public void testCreateQResult() throws Exception {
		assertNotNull(awsSqsQueueApplicationInst.createQueueResult());
	}

	public static void readPropertyFile() {
		try {
			File file = new File(".//src//main//resources//application.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();

			queueName = properties.getProperty("aws.queueName");
			awsAccessKey = properties.getProperty("aws.access_key_id");
			awsSecretAccessKey = properties.getProperty("aws.secret_access_key");
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
